# Copyright 2013 ILPS group UvA (Information and Language Processing
# Systems group University of Amsterdam) 
# 
# Licensed under the Apache License, Version 2.0 (the "License"); you
# may not use this file except in compliance with the License. You may
# obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied. See the License for the specific language governing
# permissions and limitations under the License.
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from server import app

db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    position = db.Column(db.String(255))
    area = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)

    sessions = db.relationship('Session', backref='user', lazy='dynamic')
    bookmarks = db.relationship('Bookmark', backref='user', lazy='dynamic')
    feedback = db.relationship('Feedback', backref='user', lazy='dynamic')

    __tablename__ = 'users'

    def __repr__(self):
        return '<User: %s (%s)>' % (self.username, self.id)

    def __init__(self, username, password, first_name, last_name, email,
                 position=None, area=None):
        self.username = username
        self.password = password
        self.first_name = first_name
        self.last_name = last_name
        self.position = position
        self.email = email


class Feedback(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    feedback = db.Column(db.Text)
    timestamp = db.Column(db.DateTime)

    __tablename__ = 'feedback'

    def __repr__(self):
        return '<Feedback %s>' % self.id

    def __init__(self, user_id, feedback, timestamp=None):
        self.user_id = user_id
        self.feedback = feedback
        if timestamp == None:
            timestamp = datetime.now()


class Session(db.Model):
    id = db.Column(db.String(255), primary_key=True)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    started = db.Column(db.DateTime)
    last_action = db.Column(db.DateTime)
    used = db.Column(db.Integer)

    __tablename__ = 'sessions'

    def __repr__(self):
        return '<Session %s>' % self.id

    def __init__(self, user_id, last_action, started=None):
        self.user_id = user_id
        if started is None:
            started = datetime.now()
        self.started = started
        self.last_action = last_action


class Bookmark(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    timestamp = db.Column(db.DateTime)
    doc_id = db.Column(db.String(255))
    collection = db.Column(db.String(255))
    title = db.Column(db.Text)
    description = db.Column(db.Text)

    __tablename__ = 'bookmarks'

    def __repr__(self):
        return '<Bookmark: %s, %s>' % (self.doc_id, self.user_id)

    def __init__(self, user_id, doc_id, description, collection, title,\
        timestamp=None):
        self.user_id = user_id
        if timestamp == None:
            timestamp = datetime.now()
        self.timestamp = timestamp
        self.doc_id = doc_id
        self.description = description
        self.title = title
        self.collection = collection
