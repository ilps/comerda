if (!Date.prototype.toISOString) {
    Date.prototype.toISOString = function() {
        function pad(n) { return n < 10 ? '0' + n : n; }
        return this.getUTCFullYear() + '-' +
            pad(this.getUTCMonth() + 1) + '-' +
            pad(this.getUTCDate()) + 'T' +
            pad(this.getUTCHours()) + ':' +
            pad(this.getUTCMinutes()) + ':' +
            pad(this.getUTCSeconds()) + 'Z';
    };
}

function render_document_modal(document_url, collection, doc_id){
    var m = $('<div class="modal_content" style="height:410px;"></div>');
    $(m).dialog({
        modal: true,
        width: 900,
        resizable: false,
        draggable: false,
        open: function(){
            if (ILPSLogging) {
                ILPSLogging.logEvent('document_open', {
                    doc_id: doc_id,
                    collection: collection
                });
            }

            $.get(document_url, function(data){
                $(m).html(data);
                $(m).dialog('option', 'position', 'center');
            });
        },
        close: function(event, ui){
            if (ILPSLogging) {
                ILPSLogging.logEvent('document_close', {
                    doc_id: doc_id,
                    collection: collection
                });
            }

            $(this).dialog('destroy');
            $(this).remove();
        }
    });
}

function render_pagination(n_total_hits, collection, href_target){
    var current_page;
    var pagination;
    var change_page_args;

    if(active_search_screen == 'basic'){
        current_page = current_query.basic.page;
        pagination = $('#results-' + active_search_screen + ' .pagination ul');
        change_page_args = '';
    }
    else if(active_search_screen == 'aggregated'){
        current_page = current_query.aggregated.collection[collection].page;
        pagination = $('#screen_aggregated .pagination-' + collection + ' ul');
        change_page_args = ', \'' + collection + '\'';
    }

    else if(active_search_screen == 'similarity'){
        current_page = current_query.similarity.collection[collection].page;
        pagination = $('#screen_similarity .pagination-' + collection + ' ul');
        change_page_args = ', \'' + collection + '\'';
    }

    if(!href_target){
        href_target = '#main';
    }

    // Clear possible navigation of a previous search action
    $(pagination).empty();

    // Do not attempt to render pagination when the number of hits
    // is equal or smaller than the number we are displaying on a single page
    if(n_total_hits <= PAGINATION_HITS_PER_PAGE){
        return;
    }

    var first_page = 0;
    var last_page = Math.ceil(n_total_hits / PAGINATION_HITS_PER_PAGE) - 1;

    // Previous page link
    if(current_page > 0) {
        $(pagination).append('<li class="page-'+ (current_page - 1) + '"><a data-ilpslogging-paginate="prevpage" href="' + href_target + '" onclick="change_to_page(event, ' + (current_page - 1) + change_page_args +')">&lsaquo;</a></li>');
    }

    $(pagination).append('<li class="page-0"><a href="' + href_target + '" onclick="change_to_page(event, ' + 0 + change_page_args + ')">1</a></li>');

    if(current_page > 1) {
        if(last_page > 2 && current_page > 2) {
            $(pagination).append('<li class="disabled"><a>...</a></li>');
        }

        if(current_page == last_page && last_page > 2) {
            $(pagination).append('<li class="page-'+(current_page - 2)+'"><a href="' + href_target + '" onclick="change_to_page(event, ' + (current_page - 2) + change_page_args + ')">' + (current_page - 1) + '</a></li>');
        }
        $(pagination).append('<li class="page-'+(current_page - 1)+'"><a href="' + href_target + '" onclick="change_to_page(event, ' + (current_page - 1) + change_page_args + ')">' + (current_page) + '</a></li>');
    }

    if(current_page !== 0 && current_page !== last_page) {
        $(pagination).append('<li class="page-'+current_page+'"><a href="' + href_target + '" onclick="change_to_page(event, ' + current_page + change_page_args + ')">' + (current_page + 1) + '</a></li>');
    }

    if(current_page < (last_page - 1)) {
        $(pagination).append('<li class="page-'+(current_page  + 1)+'"><a href="' + href_target + '" onclick="change_to_page(event, ' + (current_page  + 1) + change_page_args + ')">' + (current_page + 2) + '</a></li>');

        if(current_page == 1 && last_page > 3) {
            $(pagination).append('<li class="page-' + (current_page  + 2) + '"><a href="' + href_target + '" onclick="change_to_page(event, ' + (current_page  + 2) + change_page_args + ')">' + (current_page + 3) + '</a></li>');
        }
        if (last_page > 2 && current_page < (last_page - 2)) {
            $(pagination).append('<li class="disabled"><a>...</a></li>');
        }
    }

    $(pagination).append('<li class="page-' + last_page + '"><a href="' + href_target + '" onclick="change_to_page(event, ' + last_page + change_page_args + ')">' + (last_page + 1) + '</a></li>');

    if(current_page < last_page) {
        $(pagination).append('<li class="page-'+ (current_page + 1) + '"><a href="' + href_target + '" data-ilpslogging-paginate="nextpage" onclick="change_to_page(event, ' + (current_page + 1) + change_page_args +')">&rsaquo;</a></li>');
    }

    $(pagination).find('li.page-' + current_page).addClass('active');
}

function change_to_page(event, page_number, collection){
    if(active_search_screen == 'aggregated' ||  active_search_screen == 'similarity'){
        event.preventDefault();
    }

    if(ILPSLogging) {
        if(active_search_screen == 'basic'){
            ILPSLogging.paginate(current_query.basic.page, page_number);
        }
        else if (active_search_screen == 'aggregated'){
            ILPSLogging.paginate(current_query.aggregated.collection[collection].page, page_number);
        }
        else if (active_search_screen == 'similarity'){
             ILPSLogging.paginate(current_query.similarity.collection[collection].page, page_number);
        }
    }

    if(active_search_screen == 'basic'){
        current_query.basic.page = page_number;
        basic_search_query(true, false);
    }
    else if (active_search_screen == 'aggregated'){
        current_query.aggregated.collection[collection].page = page_number;
        aggregated_search_collection_query(collection, true, true, false);
    }
    else if (active_search_screen == 'similarity'){
        current_query.similarity.collection[collection].page = page_number;

        if(current_query.similarity.similar_to.docid){
            similarity_search_collection_query(collection, current_query.similarity.similar_to.collection, current_query.similarity.similar_to.docid, true, true, false);
        }
        else {
            aggregated_search_collection_query(collection, true, true, false);
        }
    }
}

function render_facets(collection, facet_id, facet_values, render_range){
    var facet_div = $('#screen_' + active_search_screen + ' .facet-' + collection + '.facet-' + facet_id);
    var facet_value_list = $(facet_div).find('ul');
    // Max number of facet values to show without clicking 'more' link
    var show_n_facet_values = 5;

    if(FACETS[facet_id].type == 'range'){
        if(render_range){
            // Do not attempt to render the slider when there are no min or max values
            if(facet_values.length === 0){
                $(facet_div).hide();
                return;
            }
            $(facet_div).show();

            // TODO: we first remove and later add the slider div in order
            // to ditch jQuery UI slider stuff from previous searches.
            // This is ugly and I want a way around this.
            $(facet_div).find('.slider').remove();
            $(facet_div).append('<div class="slider"></div>');

            // The min and max date within the available facet values
            var start_date = new Date(facet_values[0][0]);
            var end_date = new Date(facet_values[facet_values.length - 1][0]);

            // Set the initial labels of the lower and upper bounds
            $(facet_div).find('.slider-lower-val').html(start_date.toISOString().split('T')[0]);
            $(facet_div).find('.slider-upper-val').html(end_date.toISOString().split('T')[0]);

            var slider = $(facet_div).find('.slider');
            $(slider).slider({
                range: true,
                min: start_date.getTime(),
                max: end_date.getTime(),
                values: [start_date.getTime(), end_date.getTime()],
                slide: function(event, ui){
                    var start_iso = new Date(ui.values[0]).toISOString().split('T')[0];
                    var end_iso = new Date(ui.values[1]).toISOString().split('T')[0];

                    $(facet_div).find('.slider-lower-val').html(start_iso);
                    $(facet_div).find('.slider-upper-val').html(end_iso);
                },
                // Do a query when the user releases one of the drag handles
                stop: function(event, ui){
                    var start_iso = new Date(ui.values[0]).toISOString().split('T')[0];
                    var end_iso = new Date(ui.values[1]).toISOString().split('T')[0];

                    if(active_search_screen == 'basic'){
                        if(ILPSLogging) {
                            var event_props = {
                                facet: facet_id,
                                facet_value: [start_iso, end_iso],
                                collection: current_query.basic.collection
                            };

                            ILPSLogging.logEvent('filter_add', event_props);
                        }

                        current_query[active_search_screen].filters[facet_id] = [start_iso, end_iso];
                        basic_search_query(false, false);
                    }
                    else if(active_search_screen == 'aggregated'){
                        if(ILPSLogging) {
                            var event_props = {
                                facet: facet_id,
                                facet_value: [start_iso, end_iso],
                                collection: collection
                            };

                            ILPSLogging.logEvent('filter_add', event_props);
                        }

                        current_query[active_search_screen].collection[collection].filters[facet_id] = [start_iso, end_iso];
                        aggregated_search_collection_query(collection, false, true, false);
                    }
                    else if(active_search_screen == 'similarity'){
                        if(ILPSLogging) {
                            var event_props = {
                                facet: facet_id,
                                facet_value: [start_iso, end_iso],
                                collection: collection
                            };

                            ILPSLogging.logEvent('add_filter', event_props);
                        }

                        current_query[active_search_screen].collection[collection].filters[facet_id] = [start_iso, end_iso];
                        similarity_search_collection_query(collection, false, true, false);

                        if(current_query.similarity.similar_to.docid){
                            similarity_search_collection_query(collection, current_query.similarity.similar_to.collection, current_query.similarity.similar_to.docid, false, true, false);
                        }
                        else {
                            aggregated_search_collection_query(collection, false, true, false);
                        }
                    }
                }
            });
        }
    }

    if(FACETS[facet_id].type == 'checkbox'){
        var value_count = 1;

        $.each(facet_values, function(k, facet_value){
            var facet_value_class = '';

            // Initially show only the first n facets
            if(value_count > show_n_facet_values){
                facet_value_class += ' additional hide';
            }

            checkbox = '<li class="' + facet_value_class + '"><label class="checkbox"><input type="checkbox" name="' + facet_id + '" value="' + facet_value[0] + '"';

            if(active_search_screen == 'basic'){
                if(current_query.basic.filters.hasOwnProperty(facet_id)){
                    if($.inArray(facet_value[0], current_query.basic.filters[facet_id]) > -1){
                        checkbox += ' checked="checked"';
                    }
                }
            }
            else if (active_search_screen == 'aggregated'){
                if(current_query.aggregated.collection[collection].filters.hasOwnProperty(facet_id)){
                    if($.inArray(facet_value[0], current_query.aggregated.collection[collection].filters[facet_id]) > -1){
                        checkbox += ' checked="checked"';
                    }
                }
            }
            else if (active_search_screen == 'similarity'){
                if(current_query.similarity.collection[collection].filters.hasOwnProperty(facet_id)){
                    if($.inArray(facet_value[0], current_query.similarity.collection[collection].filters[facet_id]) > -1){
                        checkbox += ' checked="checked"';
                    }
                }
            }

            checkbox += '>' + facet_value[0] + ' <span class="facet_count badge">' + facet_value[1] + '</span></label></li>';
            $(facet_value_list).append(checkbox);

            value_count++;
        });

        // Add 'show more' when more than show_n_facet_values
        if(value_count > show_n_facet_values){
            $(facet_div).append('<a class="more show_more" data-ilpslogging-collection="'+collection+'" data-ilpslogging-facet="' + facet_id + '"><i class="icon-circle-arrow-down"></i> Show more...</a>');

            // Show or hide additional facet values when link is clicked
            $(facet_div).find('a.show_more').on('click', function(){
                var more = '<i class="icon-circle-arrow-down"></i> Show more...';
                var less = '<i class="icon-circle-arrow-up"></i> Show less...';

                if($(this).hasClass('more')){
                    $(this).siblings('ul').find('li.additional.hide').removeClass('hide');
                    this.innerHTML = less;
                }
                else {
                    $(this).siblings('ul').find('li.additional').addClass('hide');
                    this.innerHTML = more;
                }

                $(this).toggleClass('more less');
            });
        }
    }
}

function switch_search_screen(target_screen){
    var button;

    $('#screen_bookmarks').hide();
    $('form.form-search').show();
    $('.btn').removeClass('active btn-info');

    if(target_screen == 'basic' || target_screen == 'aggregated'){
        button = $('button#' + target_screen)[0];
    }
    else {
        button = this;
    }

    if (ILPSLogging) {
        ILPSLogging.logEvent('switch_screen', {navigate_to: this.id});

        var state = ILPSLogging.getState();
        state.active_search_screen = this.id;
        ILPSLogging.setState(state);
    }

    // Do nothing if the user hits the same button twice
    if(active_search_screen == button.id){
        $(button).addClass('btn-info active');
        $('#screen_' + active_search_screen).show();
        return;
    }

    // Highlight button of active screen
    $('.search-screen button').removeClass('btn-info active');
    $(button).addClass('btn-info active');

    $('.search_screen:not(#screen_'+ button.id + ')').hide('fast');
    $('#screen_' + button.id).removeClass('hide').show('fast');

    active_search_screen = button.id;
    current_query[active_search_screen].terms = $('#term-query-search input').val();

    if(active_search_screen == 'basic'){
        PAGINATION_HITS_PER_PAGE = 10;

        if(current_query[active_search_screen].terms){
            current_query[active_search_screen].page = 0;
            current_query[active_search_screen].filters = {};
            current_query[active_search_screen].checked_filter_values = [];
            basic_search_query();
        }
    }
    else if(active_search_screen == 'aggregated'){
        PAGINATION_HITS_PER_PAGE = 4;

        if(current_query[active_search_screen].terms){
            $.each(current_query.aggregated.collection, function(name, properties){
                properties.page = 0;
                properties.filters = {};
            });

            // Close open filter divs
            $('#screen_aggregated .facets').hide();
            $('#screen_aggregated a.filter-toggle').removeClass('filtered active');

            aggregated_search_query();
        }
    }
    else if(active_search_screen ==  'similarity'){
        PAGINATION_HITS_PER_PAGE = 4;

        if(current_query[active_search_screen].terms){
            $.each(current_query.similarity.collection, function(name, properties){
                properties.page = 0;
                properties.filters = {};
            });

            // Close open filter divs
            $('#screen_similarity .facets').hide();
            $('#screen_similarity a.filter-toggle').removeClass('filtered active');

            aggregated_search_query();
        }
    }
}

function similarity_search_query(source_collection, source_docid){
    $.each(current_query[active_search_screen].collection, function(name){
        similarity_search_collection_query(name, source_collection, source_docid, false, false, true);
    });
}

function similarity_search_collection_query(collection, source_collection, source_docid, is_paginate_action, is_facet_action, render_range){
    // Show loading gif and fade previous results
    var result_container = $('#screen_' + active_search_screen + ' .results-' + collection);
    var loading_gif = $('<div class="loading"></div>');
    $(result_container).prepend(loading_gif);

    var results = $('#screen_' + active_search_screen + ' .results-' + collection + ' ul.hits');
    $(results).fadeTo('fast', 0.5);

    if(!is_paginate_action){
        current_query[active_search_screen].collection[collection].page = 0;
    }
    
    var query_properties = {
        collection: collection,
        filters: $.extend(current_query[active_search_screen].collection[collection].filters, {}),
        page: current_query[active_search_screen].collection[collection].page,
        similar_to_doc_id: source_docid,
        similar_to_doc_collection: source_collection
    };

    if(ILPSLogging){
        ILPSLogging.query(current_query[active_search_screen].terms, query_properties);
    }

    $.post(API_URL + '/query/similarity',
        {
            'target_collection': collection,
            'source_collection': source_collection,
            'docid': source_docid,
            'page': current_query[active_search_screen].collection[collection].page,
            'filters': JSON.stringify(current_query[active_search_screen].collection[collection].filters),
            'hits_per_page': PAGINATION_HITS_PER_PAGE
        },
        function(data){
            if(ILPSLogging){
                var doc_ids = [];
                data.docs.forEach(function(doc){ doc_ids.push(doc.docid); });
                
                var state = ILPSLogging.getState();
                state.query.query_properties.collection = collection;
                ILPSLogging.setState(state);
                
                ILPSLogging.queryResults(doc_ids, data.totalhits, PAGINATION_HITS_PER_PAGE);
            }

            $(loading_gif).fadeOut().remove();

            // Remove possible hits from the same collection from a previous search
            $(results).find('li').remove();

            // Notify the user when there are no results
            $(result_container).find('.alert').remove(); // remove possible previous alerts
            $('#screen_' + active_search_screen + '_collection-' + collection + ' .num-hits').html('');

            if(data.totalhits === 0){
                $(result_container).prepend('<div class="alert alert-info no-hits-alert">No documents found that match your criteria.</div>');
                $(result_container).find('.pagination li').remove();
                return;
            }

            $('#screen_' + active_search_screen + '_collection-' + collection + ' .num-hits').html('(' + data.totalhits + ' documents)');

            // Reset the current page if this query was not initiated by a pagination click
            if(!is_paginate_action){
                current_query.aggregated.collection[collection].page = 0;

                // Remove facet values and links
                $('#screen_' + active_search_screen + ' .facet-' + collection + ' li').remove();
                $('#screen_' + active_search_screen + ' .facet-' + collection + ':not(.facet-range) a').remove();

                $.each(COLLECTIONS[collection].facets, function(k, facet){
                    render_facets(collection, facet, data.facets[facet], render_range);
                });
            }
            render_pagination(data.totalhits, collection, '#screen_' + active_search_screen + '_collection-' + collection);

            var rank = 0;
            // Append hits to results list
            $(data.docs).each(function(k, doc){
                var hit = '<li data-rank="'+rank+'" data-collection="'+collection+'" data-docid="'+doc.docid+'"  class="doctype-'+ COLLECTIONS[collection]['type'] +'">';
                if(COLLECTIONS[collection]['type'] == 'photo'){
                    if(doc.hasOwnProperty('media_path') !== false){
                        if(collection == 'tvguide'){
                            thumb_id = doc.docid.replace('tvguide_', '') + '.jpg';
                        } else {
                            thumb_id = doc.media_path[0];
                        }
                        hit += '<div class="hit-thumbnail"><a class="doc-details" data-docid="'+doc.docid+'" data-collection="'+collection+'" href="' + SITE_URL + '/document?collection=' + collection + '&docid=' + doc.docid + '&queryterms=' + encodeURIComponent(current_query.similarity.terms) + '" id="' + doc.docid + '"><img class="img-polaroid thumb_' + doc.docid + '" width="150" src="' + COLLECTIONS[collection]['media_url']  + '/t/' + doc.media_path[doc.media_path.indexOf(thumb_id)] + '"></a></div>';

                    } else {
                        // For now, don't show results that don't have an image. Fix this, or don't include in index.
                        $(hit).remove();
                        return;
                    }
                }

                hit += '<h3><a class="doc-details" data-docid="'+doc.docid+'" data-collection="'+collection+'" href="' + SITE_URL + '/document?collection=' + collection + '&docid=' + doc.docid + '&queryterms=' + encodeURIComponent(current_query.similarity.terms) + '" id="' + doc.docid + '">';
                if(doc.title !== undefined){
                    hit += doc.title;
                } else {
                    // If there is no title, use docid
                    hit += doc.docid.replace('tvguide_', '');
                }
                hit += '</a></h3>';

                if(doc.publication_date !== undefined){
                    hit += '<div class="pubdate">' + doc.publication_date + '</div>';
                }

                if(data.snippet[doc.docid] !== undefined){
                    hit += '<div class="snippet">' + data.snippet[doc.docid][collection + '_content'] + '</div>';
                }

                if(active_search_screen == 'similarity'){
                    hit += '<a class="find-similar" href="#similarity-history" data-collection="' + collection + '" data-docid="'+ doc.docid +'"><i class="icon-share-alt"></i>Find similar</a></div>';
                }

                // Bookmark link
                if(ENABLE_USER_AUTH){
                    if($.inArray(doc.docid, BOOKMARKED_DOCS) != -1){
                        hit += '<a class="btn btn-mini btn-danger bookmark delete-bookmark" href="' + SITE_URL + '/bookmark/delete?collection='+ collection + '&docid='+ doc.docid +'" data-docid="' + doc.docid + '"><i class="icon-bookmark icon-white"></i> Delete Bookmark</a>';
                    }
                    else {
                        hit += '<a class="btn btn-mini bookmark create-bookmark hide" href="' + SITE_URL + '/bookmark/create?collection='+ collection + '&docid='+ doc.docid +'" data-docid="' + doc.docid + '"><i class="icon-bookmark"></i> Bookmark</a>';
                    }
                }

                hit += '</li>';

                $(results).append(hit);

                // Hit should be present in DOM
                if(COLLECTIONS[collection]['type'] == 'photo' && doc.media_path.length > 1){
                    var media_path = doc.media_path.sort();
                    if(media_path.length > 8){
                        // limit amount of images shown in popover
                        media_path = media_path.slice(0, 8);
                    }
                    var width = 113;
                    var max_width = width * 4;
                    var popover_content = '';
                    media_path.forEach(function(img){
                        popover_content += '<div class="img-rounded popover-thumb t" style="background-image:url(' + COLLECTIONS[collection]['media_url']  + '/t/' + img + ');"></div>';

                        if (width < max_width){
                            width += width;
                        }
                    });

                    $('.hit-thumbnail img.thumb_' + doc.docid).popover({
                        placement: 'right',
                        trigger: 'hover',
                        animation: true,
                        html: true,
                        template: '<div class="popover" style="width:' + width + 'px;"><div class="arrow"></div><div class="popover-inner"><div class="popover-content" style="float:left;"><p></p></div></div></div>',
                        content: popover_content
                    });
                }
                
                rank += 1;
            });

            $(results).fadeTo('fast', 1);
        }
    );
}

function aggregated_search_query(){
    $.each(current_query[active_search_screen].collection, function(name){
        aggregated_search_collection_query(name, false, false, true);
    });
}

// Query a single collection
function aggregated_search_collection_query(collection, is_paginate_action, is_facet_action, render_range){
    // Show loading gif and fade previous results
    var result_container = $('#screen_' + active_search_screen + ' .results-' + collection);
    var loading_gif = $('<div class="loading"></div>');
    $(result_container).prepend(loading_gif);

    var results = $('#screen_' + active_search_screen + ' .results-' + collection + ' ul.hits');
    $(results).fadeTo('fast', 0.5);

    if(!is_paginate_action){
        current_query[active_search_screen].collection[collection].page = 0;
    }

    if(ILPSLogging) {
        var query_properties = {
            collection: collection,
            filters: $.extend(current_query[active_search_screen].collection[collection].filters, {}),
            page: current_query[active_search_screen].collection[collection].page
        };

        ILPSLogging.query(current_query[active_search_screen].terms, query_properties);
    }

    $.post(API_URL + '/query/basic',
        {
            'collection': collection,
            'query': current_query[active_search_screen].terms,
            'page': current_query[active_search_screen].collection[collection].page,
            'filters': JSON.stringify(current_query[active_search_screen].collection[collection].filters),
            'hits_per_page': PAGINATION_HITS_PER_PAGE
        },
        function(data){
            if(ILPSLogging){
                var doc_ids = [];
                data.docs.forEach(function(doc){ doc_ids.push(doc.docid); });
                
                var state = ILPSLogging.getState();
                state.query.query_properties.collection = collection;
                ILPSLogging.setState(state);
                
                ILPSLogging.queryResults(doc_ids, data.totalhits, PAGINATION_HITS_PER_PAGE);
            }

            $(loading_gif).fadeOut().remove();

            // Remove possible hits from the same collection from a previous search
            $(results).find('li').remove();

            // Notify the user when there are no results
            $(result_container).find('.alert').remove(); // remove possible previous alerts
            $('#screen_' + active_search_screen + '_collection-' + collection + ' .num-hits').html('');
            if(data.totalhits === 0){
                $(result_container).prepend('<div class="alert alert-info no-hits-alert">No documents found that match your criteria.</div>');
                $(result_container).find('.pagination li').remove();
                return;
            }

            $('#screen_' + active_search_screen + '_collection-' + collection + ' .num-hits').html('(' + data.totalhits + ' documents)');

            // Reset the current page if this query was not initiated by a pagination click
            if(!is_paginate_action){
                current_query.aggregated.collection[collection].page = 0;

                // Remove facet values and links
                $('#screen_' + active_search_screen + ' .facet-' + collection + ' li').remove();
                $('#screen_' + active_search_screen + ' .facet-' + collection + ':not(.facet-range) a').remove();

                $.each(COLLECTIONS[collection].facets, function(k, facet){
                    render_facets(collection, facet, data.facets[facet], render_range);
                });
            }
            render_pagination(data.totalhits, collection, '#screen_' + active_search_screen + '_collection-' + collection);

            var rank = 0;

            // Append hits to results list
            $(data.docs).each(function(k, doc){
                var hit = '<li data-rank="'+rank+'" data-collection="'+collection+'" data-docid="'+doc.docid+'" class="doctype-'+ COLLECTIONS[collection]['type'] +'">';

                if(COLLECTIONS[collection]['type'] == 'photo'){
                    if(doc.hasOwnProperty('media_path') !== false){
                        if(collection == 'tvguide'){
                            thumb_id = doc.docid.replace('tvguide_', '') + '.jpg';
                        } else {
                            thumb_id = doc.media_path[0];
                        }
                        hit += '<div class="hit-thumbnail"><a class="doc-details" data-docid="'+doc.docid+'" data-collection="'+collection+'" href="' + SITE_URL + '/document?collection=' + collection + '&docid=' + doc.docid + '&queryterms=' + encodeURIComponent(current_query[active_search_screen].terms) + '" id="' + doc.docid + '"><img class="img-polaroid thumb_' + doc.docid + '" width="150" src="' + COLLECTIONS[collection]['media_url']  + '/t/' + doc.media_path[doc.media_path.indexOf(thumb_id)] + '"></a></div>';

                    } else {
                        // For now, don't show results that don't have an image. Fix this, or don't include in index.
                        $(hit).remove();
                        return;
                    }
                }

                hit += '<h3><a class="doc-details" data-docid="'+doc.docid+'" data-collection="'+collection+'" href="' + SITE_URL + '/document?collection=' + collection + '&docid=' + doc.docid + '&queryterms=' + encodeURIComponent(current_query[active_search_screen].terms) + '" id="' + doc.docid + '">';
                if(doc.title !== undefined){
                    hit += doc.title;
                } else {
                    // If there is no title, use docid
                    hit += doc.docid.replace('tvguide_', '');
                }
                hit += '</a></h3>';

                if(doc.publication_date !== undefined){
                    hit += '<div class="pubdate">' + doc.publication_date + '</div>';
                }

                if(data.snippet[doc.docid] !== undefined){
                    hit += '<div class="snippet">' + data.snippet[doc.docid][collection + '_content'] + '</div>';
                }

                if(active_search_screen == 'similarity'){
                    hit += '<a class="find-similar" href="#" data-collection="' + collection + '" data-docid="'+ doc.docid +'"><i class="icon-share-alt"></i>Find similar</a></div>';
                }

                // Bookmark link
                if(ENABLE_USER_AUTH){
                    if($.inArray(doc.docid, BOOKMARKED_DOCS) != -1){
                        hit += '<a class="btn btn-mini btn-danger bookmark delete-bookmark" href="'+ SITE_URL +'/bookmark/delete?collection='+ collection + '&docid='+ doc.docid +'" data-docid="' + doc.docid + '"><i class="icon-bookmark icon-white"></i> Delete Bookmark</a>';
                    }
                    else {
                        hit += '<a class="btn btn-mini bookmark create-bookmark hide" href="' + SITE_URL + '/bookmark/create?collection='+ collection + '&docid='+ doc.docid +'" data-docid="' + doc.docid + '"><i class="icon-bookmark"></i> Bookmark</a>';
                    }
                }

                hit += '</li>';

                $(results).append(hit);

                // Hit should be present in DOM
                if(COLLECTIONS[collection]['type'] == 'photo' && doc.media_path.length > 1){
                    var media_path = doc.media_path.sort();
                    if(media_path.length > 8){
                        // limit amount of images shown in popover
                        media_path = media_path.slice(0, 8);
                    }
                    var width = 113;
                    var max_width = width * 4;
                    var popover_content = '';
                    media_path.forEach(function(img){
                        popover_content += '<div class="img-rounded popover-thumb" style="background-image:url(' + COLLECTIONS[collection]['media_url']  + '/t/' + img + ');"></div>';

                        if (width < max_width){
                            width += width;
                        }
                    });

                    $('.hit-thumbnail img.thumb_' + doc.docid).popover({
                        placement: 'right',
                        trigger: 'hover',
                        animation: true,
                        html: true,
                        template: '<div class="popover" style="width:' + width + 'px;"><div class="arrow"></div><div class="popover-inner"><div class="popover-content" style="float:left;"><p></p></div></div></div>',
                        content: popover_content
                    });
                }

                rank += 1;
            });

            $(results).fadeTo('fast', 1);
        }
    );
}

function show_bookmarks() {
    $('.search_screen').hide();
    $('form.form-search').hide();
    $('.btn').removeClass('active btn-info');

    if (ILPSLogging) {
        ILPSLogging.logEvent('switch_screen', {navigate_to: 'bookmarks'});

        var state = ILPSLogging.getState();
        state.active_search_screen = 'bookmarks';
        ILPSLogging.setState(state);
    }

    $(this).addClass('active btn-info');
    var bookmarks_container = $('#screen_bookmarks');
    var loading_gif = $('<div class="loading"></div>');
    $(bookmarks_container).show().prepend(loading_gif);

    var bookmarks = $('#screen_bookmarks ul');
    $(bookmarks).fadeTo('fast', 0.5);

    $.get(API_URL + '/bookmarks', function(data) {
        $(bookmarks).find('li').remove();

        $.each(data.bookmarks, function(n, bookmark) {
            $(bookmarks).append('<li class="bookmark" data-docid="'+bookmark.doc_id+'"><h3><a class="doc-details" data-docid="'+bookmark.doc_id+'" data-collection="'+bookmark.collection+'" href="' + SITE_URL + '/document?collection=' + bookmark.collection + '&docid=' + bookmark.doc_id + '" data-docid="'+bookmark.doc_id+'" data-collection="'+bookmark.collection+'">' + bookmark.title + '</a></h3><div class="bookmark-date">Bookmarked at: ' + bookmark.timestamp + '</div><div class="snippet">' + bookmark.description + '</div><a class="btn btn-mini btn-danger bookmark delete-bookmark" href="'+ SITE_URL +'/bookmark/delete?collection='+ bookmark.collection + '&docid='+ bookmark.docid +'" data-docid="' + bookmark.docid + '"><i class="icon-bookmark icon-white"></i> Delete Bookmark</a></li>');
        });

        $(loading_gif).fadeOut().remove();
        $(bookmarks).fadeTo('fast', 1);
    });
}

function change_bookmark_state(doc_id, collection, title, bookmarked) {
    $('a.bookmark[data-docid="'+doc_id+'"]').each(function() {
        var bookmark_link = this;

        if($(bookmark_link).hasClass('create-bookmark') && bookmarked === true){
            $(bookmark_link).addClass('btn-danger delete-bookmark').removeClass('create-bookmark').html('<i class="icon-bookmark icon-white"></i> Delete Bookmark').css('display', 'inline');

            // Change the action link of the bookmark to the delete endpoint
            bookmark_link.href = SITE_URL + '/bookmark/delete?docid=' + doc_id;
        }
        else if (bookmarked === false) {
            $(bookmark_link).removeClass('btn-danger delete-bookmark').addClass('create-bookmark').html('<i class="icon-bookmark"></i> Bookmark');

            bookmark_link.href = SITE_URL + '/bookmark/create?collection=' + collection + '&docid='+ doc_id;
        }
    });

    if (bookmarked) {
        // Add the bookmark to the bookmarks dropdown
        $('#bookmarks ul').prepend('<li><span class="delete"><a class="bookmark delete-bookmark" href="' + SITE_URL + '/bookmark/delete?collection=' + collection + '&docid=' + doc_id + '">x</a></span><a data-docid="'+doc_id+'" data-collection="'+collection+'" class="doc-details bookmark-' + doc_id + '" href="' + SITE_URL + '/document?collection=' + collection + '&docid=' + doc_id + '"><span class="title">' + title + '</span><span class="collection">' + COLLECTIONS[collection].name + '</span></a></li>');
        BOOKMARKED_DOCS.push(doc_id);
    } else {
        $('#bookmarks ul a[data-docid="'+doc_id+'"]').parent().parent().remove();
        $('li[data-docid="'+doc_id+'"]').remove();
        BOOKMARKED_DOCS.splice(BOOKMARKED_DOCS.indexOf(doc_id), 1);
    }
}


function basic_search_query(is_paginate_action, render_range){
    // Show a loading indicator
    var loading_gif = $('<div class="loading"></div>');
    $('body').append(loading_gif);

    var results = $('#results-basic ul.hits');
    $(results).fadeTo('fast', 0.5);

    if(!is_paginate_action){
        current_query.basic.page = 0;
    }

    if(ILPSLogging) {
        var query_properties = {
            collection: current_query.basic.collection,
            filters: $.extend(current_query.basic.filters, {}),
            page: current_query.basic.page
        };

        ILPSLogging.query(current_query.basic.terms, query_properties);
    }

    $.post(API_URL + '/query/basic',
        {
            'collection': current_query.basic.collection,
            'query': current_query.basic.terms,
            'page': current_query.basic.page,
            'filters': JSON.stringify(current_query.basic.filters)
        },
        function(data){
            if(ILPSLogging){
                var doc_ids = [];
                data.docs.forEach(function(doc){ doc_ids.push(doc.docid); });
                ILPSLogging.queryResults(doc_ids, data.totalhits, PAGINATION_HITS_PER_PAGE);
            }

            // Remove the loading indicator
            $(loading_gif).fadeOut().remove();

            // Remove possible results (hits) from previous query
            $(results).find('li').remove();

            // Reset the current page if this query was not initiated by a pagination click
            // also (re-)render facets
            if(!is_paginate_action){
                // Hide all facets with checkboxes (except the collection facet)
                $('#facets_basic .facet:not(.facet-collections).facet-checkbox').addClass('hide');

                // Remove facet values and links
                $('#facets_basic .facet:not(.facet-collections).facet-checkbox li').remove();
                $('#facets_basic .facet:not(.facet-collections).facet-checkbox a').remove();

                // Show relevant facets for this collection
                $('#facets_basic .facet-' + current_query.basic.collection + '.facet-checkbox').removeClass('hide');

                if(render_range){
                    $('#facets_basic .facet:not(.facet-collections).facet-range').addClass('hide');
                    $('#facets_basic .facet:not(.facet-collections).facet-range li').remove();
                    $('#facets_basic .facet:not(.facet-collections).facet-range a').remove();
                    $('#facets_basic .facet-' + current_query.basic.collection + '.facet-range').removeClass('hide');
                }

                $.each(COLLECTIONS[current_query.basic.collection].facets, function(k, facet){
                    render_facets(current_query.basic.collection, facet, data.facets[facet], render_range);
                });
            }
            render_pagination(data.totalhits);

            // Notify the user when there are no results
            $('#results-basic .alert').remove(); // remove possible previous alerts
            $('#results-basic .num-hits').remove();
            if(data.totalhits === 0){
                $('#results-basic').prepend('<div class="alert alert-info no-hits-alert">No documents found that match your criteria.</div>');
                $(result_container).find('.pagination li').remove();
                return;
            }

            $('#results-basic').prepend('<div class="num-hits">Found ' + data.totalhits + ' documents</div>');

            var rank = 0;

            // Append hits to results list
            $(data.docs).each(function(k, doc){
                var hit = '<li data-rank="'+rank+'" data-collection="'+current_query.basic.collection+'" data-docid="'+doc.docid+'" class="doctype-'+ COLLECTIONS[current_query.basic.collection]['type'] +'">';

                if(COLLECTIONS[current_query.basic.collection]['type'] == 'photo'){
                    if(doc.hasOwnProperty('media_path') !== false){
                        if(current_query.basic.collection == 'tvguide'){
                            thumb_id = doc.docid.replace('tvguide_', '') + '.jpg';
                        } else {
                            thumb_id = doc.media_path[0];
                        }
                        hit += '<div class="hit-thumbnail"><a class="doc-details" data-docid="'+doc.docid+'" data-collection="'+current_query.basic.collection+'" href="' + SITE_URL + '/document?collection=' + current_query.basic.collection + '&docid=' + doc.docid + '&queryterms=' + encodeURIComponent(current_query.basic.terms) + '" id="' + doc.docid + '"><img class="img-polaroid thumb_' + doc.docid + '" width="150" src="' + COLLECTIONS[current_query.basic.collection]['media_url']  + '/t/' + doc.media_path[doc.media_path.indexOf(thumb_id)] + '"></a></div>';
                    } else {
                        // For now, don't show results that don't have an image. Fix this, or don't include in index.
                        $(hit).remove();
                        return;
                    }
                }

                hit += '<h3><a class="doc-details" data-docid="'+doc.docid+'" data-collection="'+current_query.basic.collection+'" href="' + SITE_URL + '/document?collection=' + current_query.basic.collection + '&docid=' + doc.docid + '&queryterms=' + encodeURIComponent(current_query.basic.terms) + '" id="' + doc.docid + '">';
                if(doc.title !== undefined){
                    hit += doc.title;
                } else {
                    // If there is no title, use docid
                    hit += doc.docid.replace('tvguide_', '');
                }
                hit += '</a></h3>';

                if(doc.publication_date !== undefined){
                    hit += '<div class="pubdate">' + doc.publication_date + '</div>';
                }

                if(data.snippet[doc.docid] !== undefined){
                    hit += '<div class="snippet">' + data.snippet[doc.docid][current_query.basic.collection + '_content'] + '</div>';
                }

                // Bookmark link
                if(ENABLE_USER_AUTH){
                    if($.inArray(doc.docid, BOOKMARKED_DOCS) != -1){
                        hit += '<a class="btn btn-mini btn-danger bookmark delete-bookmark" href="' + SITE_URL + '/bookmark/delete?collection='+ current_query.basic.collection + '&docid='+ doc.docid +'" data-docid="' + doc.docid + '"><i class="icon-bookmark icon-white"></i> Delete Bookmark</a>';
                    }
                    else {
                        hit += '<a class="btn btn-mini bookmark create-bookmark hide" href="' + SITE_URL + '/bookmark/create?collection='+ current_query.basic.collection + '&docid='+ doc.docid +'" data-docid="' + doc.docid + '"><i class="icon-bookmark"></i> Bookmark</a>';
                    }
                }

                hit += '</li>';

                $(results).append(hit);

                // Hit should be present in DOM
                if(COLLECTIONS[current_query.basic.collection]['type'] == 'photo' && doc.media_path.length > 1){
                    var media_path = doc.media_path.sort();
                    if(doc.media_path.length > 8){
                        // limit amount of images shown in popover
                        media_path = media_path.slice(0, 8);
                    }
                    var width = 113;
                    var max_width = width * 4;
                    var popover_content = '';
                    media_path.forEach(function(img){
                        popover_content += '<div class="img-rounded popover-thumb t" style="background-image:url(' + COLLECTIONS[current_query.basic.collection]['media_url']  + '/t/' + img + ');"></div>';

                        if (width < max_width){
                            width += width;
                        }
                    });

                    $('.hit-thumbnail img.thumb_' + doc.docid).popover({
                        placement: 'right',
                        trigger: 'hover',
                        animation: true,
                        html: true,
                        template: '<div class="popover" style="width:' + width + 'px;"><div class="arrow"></div><div class="popover-inner"><div class="popover-content" style="float:left;"><p></p></div></div></div>',
                        content: popover_content
                    });
                }

                rank += 1;
            });

            // Fade the results list back to 100%
            $(results).fadeTo('fast', 1);
        }
    );
}

function disable_facets(screen_name, collection) {
    if(ILPSLogging) {
        ILPSLogging.logEvent('disable_facets', {
            search_screen: screen_name
        });

        var state = ILPSLogging.getState();
        state.fasets_disabled = false;
        ILPSLogging.setState(state);
    }

    if(screen_name == 'basic') {
        // Hide the facets panel
        $('#facets_basic').hide();

        // Make the hits panel 100% in width
        $('#results-basic').addClass('full-width');

        if(collection !== undefined){
            current_query['basic'].terms = $('#term-query-search input').val();
            current_query['basic'].page = 0;
            current_query['basic'].filters = {};
            basic_search_query(false, true);
        }
    }
    else {
        $('#screen_' + screen_name + ' .filter-toggle').hide();
        $('#screen_' + screen_name + ' .facets').hide();
    }
}

function enable_facets(screen_name, collection) {
    if(ILPSLogging) {
        ILPSLogging.logEvent('enable_facets', {
            search_screen: screen_name
        });

        var state = ILPSLogging.getState();
        state.fasets_disabled = false;
        ILPSLogging.setState(state);
    }

    if(screen_name == 'basic') {
        $('#facets_basic').show();
        $('#results-basic').removeClass('full-width');
    }
    else {
        $('#screen_' + screen_name + ' .filter-toggle').show();
    }
}

function switch_collection(search_screen, event){
    // Set the terms of the query always to whatever is in the input box. This
    // will cause a query on those terms when a user switches collections.
    // This ONLY works for facet values that are consistent accross queries,
    // in this case for the collections ("Mies Bouwman" will have very
    // different names associated than "Aart Staartjes")
    current_query[search_screen].terms = $('#term-query-search input').val();

    if(event.type == 'radio'){
        current_query[search_screen].collection = event.value;
    }

    // If this is a checkbox we need all checked collections in an array
    if(event.type == 'checkbox'){
        var collections = [];
        $('#facets_' + search_screen + ' .facet-collections input:checked').each(function(k, collection){
            collections.push(collection.value);
        });

        current_query[search_screen].collections = collections;
    }

    // When the collection is changed, set the page to zero and reset filters
    current_query[search_screen].page = 0;
    current_query[search_screen].filters = {};

    if(search_screen == 'basic'){
        basic_search_query(false, true);
    }
}

$(document).ready(function(){
    // By default a user starts with the basic search screen and no query is entered
    active_search_screen = 'basic';
    current_query = {
        'basic': {
            'terms': null,
            'collection': $('#facets_' + active_search_screen + ' .facet-collections input:checked:first').val(),
            'filters': {},
            'page': 0,
            'checked_filter_values': []
        },
        'aggregated': {
            'terms': null,
            'collection': {}
        },
        'similarity': {
            'terms': null,
            'collection': {},
            'similar_to': {
                'docid': null,
                'collection': null
            }
        }
    };

    $.each(COLLECTIONS, function(name, properties){
        current_query.aggregated.collection[name] = {
            'filters': {},
            'page': 0
        };

        current_query.similarity.collection[name] = {
            'filters': {},
            'page': 0
        };
    });

    // Fired when the user switches from search screen
    $('.search-screen button').click(switch_search_screen);

    // Fired when the user clicks the search button or hits enter in the input box
    $('#term-query-search').on('submit', function(event){
        event.preventDefault();
        terms = $(this).find('input').val();

        if(ILPSLogging) {
            ILPSLogging.logEvent('query_input_submit', {query_string: terms});
        }

        current_query[active_search_screen].terms = terms;

        // When a new query is entered, set the page to zero and reset possible
        // facet filters, than fire the actual query
        if(active_search_screen == 'basic'){
            current_query[active_search_screen].page = 0;
            current_query[active_search_screen].filters = {};
            current_query[active_search_screen].checked_filter_values = [];
            basic_search_query(false, true);
        }
        else if(active_search_screen == 'aggregated'){
            $.each(current_query.aggregated.collection, function(name, properties){
                properties.page = 0;
                properties.filters = {};
            });

            $('#screen_aggregated .facets').hide();
            $('#screen_aggregated a.filter-toggle').removeClass('active filtered');
            aggregated_search_query();
        }

        else if(active_search_screen == 'similarity'){
            current_query.similarity.similar_to.collection = null;
            current_query.similarity.similar_to.docid = null;

            $.each(current_query.similarity.collection, function(name, properties){
                properties.page = 0;
                properties.filters = {};
            });

            $('#similarity-history').slideUp('fast', function(){
                $('#similarity-history li').remove();
            });
            $('#screen_similarity .facets').hide();
            $('#screen_similarity a.filter-toggle').removeClass('active filtered');
            aggregated_search_query();
        }
    });

    // Fired when the user changes the selected collections
    $('.facet-collections input').on('change', function(event){
        if(ILPSLogging) {
            // Delete facets enabled/disabled indicator from state
            var state = ILPSLogging.getState();
            if('facets_enabled' in state) {
                delete state.facets_enabled;
                ILPSLogging.setState(state);
            }

            ILPSLogging.logEvent('switch_to_collection', {
                from_colletion: current_query.basic.collection,
                to_collection: event.value
            });
        }
        switch_collection(active_search_screen, this);
    });

    // On click of a doc details link, render a modal containg the document
    $('a.doc-details').live('click', function(event){
        render_document_modal(this.href, $(this).data('collection'), $(this).data('docid'));
        return false;
    });

    // Change filters and fire query when facet values are clicked
    $('.facet input:not(#collection)').live('change', function(){
        if(active_search_screen == 'basic'){
            $('#screen_basic input:not(#collection)').attr('disabled', 'disabled');

            current_query[active_search_screen].filters = {};

            $('#screen_' + active_search_screen + ' input:checked:not(#collection)').each(function(k, facet_value){
                if (current_query[active_search_screen].filters[this.name] === undefined){
                    current_query[active_search_screen].filters[this.name] = [];
                }
                current_query[active_search_screen].filters[this.name].push(this.value);
            });

            if(ILPSLogging) {
                var event_props = {
                    facet: this.name,
                    facet_value: this.value,
                    collection: current_query.basic.collection
                };

                if(this.checked) {
                    ILPSLogging.logEvent('filter_add', event_props);
                } else {
                    ILPSLogging.logEvent('filter_remove', event_props);
                }
            }

            basic_search_query(false, false);
        }
        else if(active_search_screen == 'aggregated'){
            // Very ugly way to get the collection of the clicked facet
            var collection = $(this).parents('.facet').attr('class').split(' ')[1].replace('facet-', '');
            $('#screen_aggregated .facet-' + collection + ' input').attr('disabled', 'disabled');

            var n_filters = 0;
            current_query.aggregated.collection[collection].filters = {};
            $('#screen_' + active_search_screen + ' .facet-'+ collection + ' input:checked:not(#collection)').each(function(k, facet_value){
                if (current_query.aggregated.collection[collection].filters[this.name] === undefined){
                    current_query.aggregated.collection[collection].filters[this.name] = [];
                }

                current_query.aggregated.collection[collection].filters[this.name].push(this.value);

                n_filters++;
            });

            var filter_toggle = $('.collection-' + collection + ' a.filter-toggle');
            if(n_filters === 0){
                $(filter_toggle).removeClass('filtered');
            }
            else {
                $(filter_toggle).addClass('filtered');
            }

            if(ILPSLogging) {
                var event_props = {
                    facet: this.name,
                    facet_value: this.value,
                    collection: collection
                };

                if(this.checked) {
                    console.log(this);
                    ILPSLogging.logEvent('filter_add', event_props);
                } else {
                    ILPSLogging.logEvent('filter_remove', event_props);
                }
            }

            aggregated_search_collection_query(collection, false, true, false);
        }
        else if(active_search_screen == 'similarity'){
            var collection = $(this).parents('.facet').attr('class').split(' ')[1].replace('facet-', '');
            $('#screen_similarity .facet-' + collection + ' input').attr('disabled', 'disabled');

            var n_filters = 0;
            current_query.similarity.collection[collection].filters = {};
            $('#screen_' + active_search_screen + ' .facet-'+ collection +' input:checked:not(#collection)').each(function(k, facet_value){
                if (current_query.similarity.collection[collection].filters[this.name] === undefined){
                    current_query.similarity.collection[collection].filters[this.name] = [];
                }

                current_query.similarity.collection[collection].filters[this.name].push(this.value);

                n_filters++;
            });

            var filter_toggle = $('.collection-' + collection + ' a.filter-toggle');
            if(n_filters === 0){
                $(filter_toggle).removeClass('filtered');
            }
            else {
                $(filter_toggle).addClass('filtered');
            }

            if(ILPSLogging) {
                var event_props = {
                    facet: this.name,
                    facet_value: this.value,
                    collection: collection
                };

                if(this.checked) {
                    ILPSLogging.logEvent('filter_add', event_props);
                } else {
                    ILPSLogging.logEvent('filter_remove', event_props);
                }
            }

            if(current_query.similarity.similar_to.docid){
                similarity_search_collection_query(collection, current_query.similarity.similar_to.collection , current_query.similarity.similar_to.docid, false, true, false);
            }
            else {
                aggregated_search_collection_query(collection, false, true, false);
            }
        }
    });

    // Toggle facets when a collection's 'filter' link is clicked
    $('a.filter-toggle').on('click', function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');

            if(ILPSLogging) {
                var collection = $(this).parents('.collection').attr('class').split(' ')[1].replace('collection-', '');
                ILPSLogging.logEvent('filters_hide', {collection: collection});
            }
        }
        else {
            $(this).addClass('active');
            
            if(ILPSLogging) {
                var collection = $(this).parents('.collection').attr('class').split(' ')[1].replace('collection-', '');
                ILPSLogging.logEvent('filters_show', {collection: collection});
            }
        }

        $(this).parents('.collection').find('.facets').toggle();
    });

    // Only show the bookmark links when the user hovers over a hit
    $('.hits li').live({
        mouseenter: function(){
            $(this).find('.create-bookmark').show();
        },
        mouseleave: function(){
            $(this).find('.create-bookmark').hide();
        }
    });

    // When a bookmark create or delete button (displayed for each hit) is clicked
    $('a.bookmark').live('click', function(event){
        event.preventDefault();

        var bookmark_link = this;
        if($(bookmark_link).hasClass('create-bookmark')) {
            if(BOOKMARKED_DOCS.indexOf($(bookmark_link).data('docid')) !== -1){
                return;
            }
        }
        else {
            if(BOOKMARKED_DOCS.indexOf($(bookmark_link).data('docid')) === -1){
                return;
            }
        }

        $.get(this.href, function(data){
            // Turn the link into a delete link if we just created a bookmark
            if($(bookmark_link).hasClass('create-bookmark')){
                change_bookmark_state(data.doc_id, data.collection, data.title, true);

                if(ILPSLogging) {
                    ILPSLogging.logEvent('bookmark_add', {
                        doc_id: data.doc_id,
                        collection: data.collection
                    });
                }
            }
            else {
                change_bookmark_state(data.doc_id, data.collection, data.title, false);
                if(ILPSLogging) {
                    ILPSLogging.logEvent('bookmark_delete', {
                        doc_id: data.doc_id,
                        collection: data.collection
                    });
                }
            }
        });
    });

    // When a 'find similar' link is clicked (displayed in 'similarity' screen)
    $('a.find-similar').live('click', function(event){
        $('#similarity-history').slideDown();
        $.each(current_query.similarity.collection, function(name, properties){
                properties.page = 0;
                properties.filters = {};
        });

        $('a.filter-toggle').removeClass('filtered');

        if($('#similarity-history ul li').size() == PAGINATION_HITS_PER_PAGE){
            $('#similarity-history ul li:last').fadeOut('fast', function(){
                $(this).remove();
            });
        }

        $('#similarity-history ul li').fadeTo('slow', 0.50).removeClass('active');

        var item_html = $(this).parents('li');

        if($(item_html).hasClass('doctype-photo')){
            item_html = $(item_html).clone();
            $(item_html).find('.snippet').remove();
            item_html = $(item_html).html();
        }
        else {
            item_html = $(item_html).html();
        }

        $('#similarity-history ul').prepend('<li class="active">' + item_html + '</li>');

        current_query.similarity.similar_to.collection = $(this).attr('data-collection');
        current_query.similarity.similar_to.docid = $(this).attr('data-docid');

        if(ILPSLogging) {
            ILPSLogging.logEvent('find_similar', {
                doc_id: current_query.similarity.similar_to.docid,
                collection: current_query.similarity.similar_to.collection
            });
        }

        similarity_search_query(current_query.similarity.similar_to.collection, current_query.similarity.similar_to.docid);
    });

    $('#similarity-history ul li:not(.active)').live({
        mouseenter: function(){
            $(this).stop(true, true).fadeTo('normal', 1);
        },
        mouseleave: function(){
            $(this).stop(true, true).fadeTo('normal', 0.5);
        }
    });

    // Log hovering over hits
    var hover_duration = {};
    $('ul.hits li').live({
        mouseenter: function() {
            hover_duration[$(this).data('docid')] = new Date().getTime();
        },
        mouseleave: function() {
            var end_time = new Date().getTime();
            var duration = end_time - hover_duration[$(this).data('docid')];
            var data = $(this).data();

            if(ILPSLogging) {
                ILPSLogging.logEvent('hit_hover', {
                    rank: data.rank,
                    hover_duration: duration,
                    doc_id: data.docid,
                    collection: data.collection
                });
            }
        }
    });
    
    $('a#show_bookmarks').click(show_bookmarks);
});
