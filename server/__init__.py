# Copyright 2013 ILPS group UvA (Information and Language Processing
# Systems group University of Amsterdam) 
# 
# Licensed under the Apache License, Version 2.0 (the "License"); you
# may not use this file except in compliance with the License. You may
# obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied. See the License for the specific language governing
# permissions and limitations under the License.
from datetime import datetime
import json

from flask import Flask, render_template, request, redirect, session, jsonify
from werkzeug.contrib.fixers import ProxyFix
import requests
import sunburnt

app = Flask(__name__)
app.config.from_object('server.settings.Config')

if app.config['ENABLE_SENTRY_LOGGING']:
    from raven.contrib.flask import Sentry
    sentry = Sentry(app, dsn=app.config['SENTRY_DSN'])

from models import *

# Collect all schemas to prevent unnecessary HTTP requests
for collection_name in app.config['COLLECTIONS'].keys():
    remote_schema = '%s/%s/%s' % (app.config['SOLR_URL'], collection_name,\
                                  'admin/file/?file=schema.xml')
    r = requests.get(remote_schema)
    if r.text:
        with open('%s/%s.xml' % (app.config['SCHEMA_DIR'], collection_name), 'w') as f:
            f.write(r.text)

# SOLR_INTERFACES = {collection_name: sunburnt.SolrInterface('%s/%s' %\
#     (app.config['SOLR_URL'], collection_name))\
#     for collection_name in app.config['COLLECTIONS'].keys()}


@app.context_processor
def inject_settings():
    """Inject the global settings into the template context."""
    return dict(settings=app.config)


@app.route('/')
def home():
    user_id = None
    if not app.config['ENABLE_USER_AUTH']:
        return render_template('index.html', current_user_id=user_id)
    else:
        if 'session_id' not in session:
            session_id = request.args.get('session_id', None)
            if not session_id:
                # No session id provided
                return redirect(app.config['REDIRECT_LOGIN_URL'])

            user_session = Session.query.get(session_id)
            if user_session:
                if user_session.used != 0:
                    # If the session is used, redirect to login
                    return redirect(app.config['REDIRECT_LOGIN_URL'])
                session['session_id'] = session_id
                session['user_id'] = user_session.user_id
                user_id = session['user_id']

                # Update session, so it cannot be reused by someone else
                now = datetime.now()
                user_session.used = 1
                user_session.started = now
                user_session.last_action = now

                db.session.add(user_session)
                db.session.commit()
            else:
                return redirect(app.config['REDIRECT_LOGIN_URL'])

        return render_template('index.html',
                               bookmarks=get_bookmarks(session['user_id']),
                               current_user_id=user_id)


@app.route('/logout')
def logout():
    """Redirects the user to the logout page of the user management application,
    and destroys the session."""

    for key in session.keys():
        del(session[key])

    return redirect(app.config['REDIRECT_LOGOUT_URL'])


@app.route('/document')
def get_document():
    collection = request.args.get('collection', None)
    docid = request.args.get('docid', None)
    # Default to empty string, to make sure that bookmarks are rendered (bookmarks
    # and the query they are bookmarked on are unrelated)
    queryterms = request.args.get('queryterms', '')

    if not docid or not collection:
        return jsonify(error='You should provide a docid, a collection and the query terms.')
    solr = get_solr_interface(collection)
    query = solr.query(**{'%s_docid' % (collection): docid})

    # Add query terms for highlighting
    for term in queryterms.split():
        query = query.query(text=term)

    # Specify the fields to highlight
    query = query.highlight([field % collection for field in app.config['HIGHLIGHT_FIELDS']], fragsize=100000)

    response = query.execute()
    if not response:
        response = solr.query(**{'%s_docid' % (collection): docid}).execute()

    response = response[0]
    doc = {}

    for field in app.config['DOCUMENT_VIEW_FIELDS']:
        result_field = '%s_%s' % (collection, field)
        if 'solr_highlights' in response and result_field in response['solr_highlights']:
            doc[field] = response['solr_highlights'][result_field]
        elif result_field in response:
            doc[field] = response[result_field]

    # # We don't need to return datetimes or timestamp, so format time here
    if 'publication_date' in doc and doc['publication_date']:
        doc['publication_date'] = doc['publication_date'].strftime('%b. %d, %Y')

    # If usage logging is enabled, check if this item is bookmarked by
    # the user that is requesting it.
    bookmarked = False
    if 'user_id' in session:
        if Bookmark.query.filter_by(doc_id=docid, user_id=session['user_id'])\
            .first():
            bookmarked = True

    return render_template(
        'modal_%s.html' % (collection),
        doc=doc,
        collection=collection,
        docid=docid,
        bookmarked=bookmarked
    )

@app.route('/bookmarks', methods=['GET'])
def bookmarks():
    bookmarks = get_bookmarks(session['user_id'])

    return jsonify(bookmarks=bookmarks)


@app.route('/bookmark/create', methods=['GET'])
def create_bookmark():
    docid = request.args.get('docid', None)
    collection = request.args.get('collection', None)

    if not docid or not collection:
        return jsonify(error='You should provide a docid and a collection')

    bookmark = Bookmark.query.filter_by(doc_id=docid,\
        user_id=session['user_id']).first()
    if bookmark:
        return jsonify(format_bookmark(bookmark.__dict__))

    solr = get_solr_interface(collection)
    query = solr.query(**{'%s_docid' % (collection): docid})

    result = query.execute()[0]
    description = result['%s_content_summary' % collection][0]
    if '%s_title' % (collection) in result:
        title = result['%s_title' % collection]
    else:
        title = docid

    bookmark_data = {
        'user_id': session['user_id'],
        'timestamp': datetime.now(),
        'doc_id': docid,
        'description': description,
        'collection': collection,
        'title': title
    }

    bookmark = Bookmark(**bookmark_data)

    db.session.add(bookmark)
    db.session.commit()

    bookmark_data['timestamp'] = bookmark_data['timestamp']\
        .strftime('%b. %d, %Y')

    bookmark_data['id'] = bookmark.id

    return jsonify(bookmark_data)


@app.route('/bookmark/delete', methods=['GET'])
def delete_bookmark():
    docid = request.args.get('docid', None)
    if not docid:
        return jsonify(error='You should specify a bookmark id')

    bookmark = Bookmark.query.filter_by(doc_id=docid,\
        user_id=session['user_id']).first()

    bookmark_data = bookmark.__dict__
    db.session.delete(bookmark)
    db.session.commit()

    return jsonify(format_bookmark(bookmark_data))


def format_bookmark(bookmark_data):
    """Remove SQL-alchemy stuff, and format date."""
    result = {}
    for field in bookmark_data:
        if field != '_sa_instance_state':
            if type(bookmark_data[field]) == datetime:
                result[field] = bookmark_data[field].strftime('%b. %d, %Y')
            else:
                result[field] = bookmark_data[field]

    return result


@app.route('/query/basic', methods=['POST'])
def query():
    """
    Performs a query on a given collection, and when given a page number,
    returns the appropriate results for that page.

    Query is the query string entered by the user. Note that for now, the query
    string is not parsed at all: "mies bouwman" is a phrase query, not "mies"
    AND "bouwman".

    Collection is the specific collection that is searched. Collections
    available for search are specified in the COLLECTIONS in settings.py. Note
    that there should be a Solr collection with the same name.

    Page is the result page requested: the amount of results per page is
    specified in settings.py.

    Filters should be provided as a JSON object, where the attribute name
    represents the field that should be filtered on, and the attribute value is
    an array of values to filter on. Note that the value should ALWAYS be an
    array:
    {
        'keywords': ['Terrorisme', 'Ziekten'],
        'names': ['europa']
    }
    """
    def _construct_filter_query(field, field_values):
        """
        Recursive function for generating OR queries for multiple field values.
        As we do not know beforehand how many values for a specific field are
        specified by the user, a recursive solution for generating the
        appropriate OR query is necessary.
        """
        si = get_solr_interface(collection)
        if len(field_values) == 1:
            # If there is only one filter value, return query object
            return si.Q(**{field: field_values[0]})
        if len(field_values) == 2:
            # End condition; two filter values found, return query object
            return si.Q(si\
                .Q(**{field: field_values[0]}) | si.Q(**{field:\
                    field_values[1]}))
        else:
            # End condition not met; create query object with call to self and
            # remaining filter values
            return si.Q(si\
                .Q(**{field: field_values[0]}) | si.Q(_construct_filter_query(\
                    field, field_values[1:])))

    query = request.form.get('query', None)
    collection = request.form.get('collection', None)
    hits_per_page = int(request.form.get('hits_per_page',\
        app.config['PAGINATION_HITS_PER_PAGE']))
    # In order to facilitate 'go to page 3', calculate starting position here
    page = int(request.form.get('page', 0)) * hits_per_page
    filters = request.form.get('filters', None)
    sort = request.form.get('sort', '-score')

    if not query or not collection or collection not in app.config['COLLECTION_ORDER']:
        return jsonify(error='You should provide a query and a collection '
                'that has an index.')

    terms = query.split()
    query = get_solr_interface(collection).query(text=terms[0])

    # If we have more than one term, AND those to the first term
    if len(terms) > 1:
        for term in terms[1:]:
            query = query.query(text=term)

    # Add additional options such as highlighting, pagination and sorting
    query = query\
                .highlight([field % collection\
                    for field in app.config['HIGHLIGHT_FIELDS']], fragsize=180,
                    alternateField='text', maxAlternateFieldLength=180)\
                .paginate(start=page,
                    rows=hits_per_page)\
                .sort_by(sort)

    facets = []
    range_facets = []
    for facet in app.config['COLLECTIONS'][collection]['facets']:
        if app.config['FACETS'][facet]['type'] != 'range':
            facets.append(facet)
        else:
            range_facets.append(facet)

    query = query.facet_by(facets, mincount=1, sort='count')
    # we need to find ALL dates, therefore limit=-1
    query = query.facet_by(range_facets, mincount=1, sort='index', limit=-1)

    if filters:
        filters = json.loads(filters)
        for range_facet in range_facets:
            if range_facet in filters:
                # User filter rather than query, for caching
                query = query.query(**{'%s__range' % (range_facet):\
                    filters[range_facet]})
                del filters[range_facet]

        for field, field_values in filters.iteritems():
            query = query.filter(_construct_filter_query(field, field_values))

    response = query.execute(constructor=search_view)
    if response:
        # as we only need the first and last date, replace the range_facet
        # values with just those (10x less JSON over the wire)
        for range_facet in range_facets:
            if response.facet_counts.facet_fields[range_facet]:
                response.facet_counts.facet_fields[range_facet] = [
                    response.facet_counts.facet_fields[range_facet][0],
                    response.facet_counts.facet_fields[range_facet][-1]
                ]
            else:
                response.facet_counts.facet_fields[range_facet] = []

    return jsonify(docs=response.result.docs,
                   totalhits=response.result.numFound,
                   snippet=response.highlighting,
                   facets=response.facet_counts.facet_fields)


@app.route('/query/similarity', methods=['POST'])
def similarity_query():
    """
    Based on the text in a document, performs a similarity search by constructing
    an OR-query based on 200 unique terms from that document.

    Source and target collection are required in order to obtain the proper document,
    and query the appropriate collection from which we require similar documents.

    Hits per page allows us to specify how many documents we actually want to
    retrieve, and defaults to the amount specified in the settings.

    Page allows for pagination.

    Filters is for faceting.
    """
    def _generate_or_query(terms, field, collection):
        si = get_solr_interface(collection)
        if len(terms) == 1:
            return si.Q(**{field: terms[0]})
        if len(terms) == 2:
            return si.Q(si.Q(**{field: terms[0]}) | si.Q(**{field: terms[1]}))
        else:
            return si.Q(si.Q(**{field: terms[0]}) |\
                _generate_or_query(terms[1:], field, collection))

    def _construct_filter_query(field, field_values, collection):
        """
        Recursive function for generating OR queries for multiple field values.
        As we do not know beforehand how many values for a specific field are
        specified by the user, a recursive solution for generating the
        appropriate OR query is necessary.

        Also, flagrant DRY violation. Please refactor at some point.
        """
        si = get_solr_interface(collection)
        if len(field_values) == 1:
            # If there is only one filter value, return query object
            return si.Q(**{field: field_values[0]})
        if len(field_values) == 2:
            # End condition; two filter values found, return query object
            return si.Q(si\
                .Q(**{field: field_values[0]}) | si.Q(**{field:\
                    field_values[1]}))
        else:
            # End condition not met; create query object with call to self and
            # remaining filter values
            return si.Q(si\
                .Q(**{field: field_values[0]}) | si.Q(_construct_filter_query(\
                    field, field_values[1:], collection)))

    docid = request.form.get('docid', None)
    source_collection = request.form.get('source_collection', None)
    target_collection = request.form.get('target_collection', None)
    hits_per_page = int(request.form.get('hits_per_page',\
        app.config['PAGINATION_HITS_PER_PAGE']))
    page = int(request.form.get('page', 0)) * hits_per_page
    filters = request.form.get('filters', None)

    if not docid or not source_collection or not target_collection or\
        source_collection not in app.config['COLLECTION_ORDER'] or\
        source_collection not in app.config['COLLECTION_ORDER']:
        return jsonify(error='You should provide a docid, the collection of '
                             'that document, and the collection you want to '
                             'obtain similar documents from. Also, these '
                             'collections should be in an index.')

    source_solr = get_solr_interface(source_collection)
    text = source_solr.query(**{'%s_docid' %\
        (source_collection): docid}).field_limit('text').execute()

    terms = list(set([term for term in ' '.join(text.result.docs[0]['text']).split()\
        if term.isalpha()]))[:200]
    if not terms:
        return jsonify(error='No terms were found')

    if source_collection == target_collection:
        field = 'text'
    else:
        field = '%s_%s_expanded' % (target_collection, source_collection)

    target_solr = get_solr_interface(target_collection)
    query = target_solr\
            .query(_generate_or_query(terms, field, target_collection))\
            .highlight([field % target_collection\
                    for field in app.config['HIGHLIGHT_FIELDS']], fragsize=180)\
            .paginate(start=page, rows=hits_per_page)

    facets = []
    range_facets = []
    for facet in app.config['COLLECTIONS'][target_collection]['facets']:
        if app.config['FACETS'][facet]['type'] != 'range':
            facets.append(facet)
        else:
            range_facets.append(facet)

    query = query.facet_by(facets, mincount=1, sort='count')
    query = query.facet_by(range_facets, mincount=1, sort='index', limit=-1)

    if filters:
        filters = json.loads(filters)
        for range_facet in range_facets:
            if range_facet in filters:
                # User filter rather than query, for caching
                query = query.query(**{'%s__range' % (range_facet):\
                    filters[range_facet]})
                del filters[range_facet]

        for field, field_values in filters.iteritems():
            query = query.filter(_construct_filter_query(field, field_values,\
                                                            target_collection))

    response = query.execute(constructor=search_view)

    if response:
        for range_facet in range_facets:
            if response.facet_counts.facet_fields[range_facet]:
                response.facet_counts.facet_fields[range_facet] = [
                    response.facet_counts.facet_fields[range_facet][0],
                    response.facet_counts.facet_fields[range_facet][-1]
                ]
            else:
                response.facet_counts.facet_fields[range_facet] = []

    return jsonify(docs=response.result.docs,
                   totalhits=response.result.numFound,
                   facets=response.facet_counts.facet_fields,
                   snippet=response.highlighting)


# We can't pass variables other that the SolrResult, so we need different
# constructor functions for different results.
def search_view(**result_dict):
    """
    As we do not need to return all values stored in the index (text, for
    instance), this function acts like a constructor for the format of a
    query result. The EXPOSED_FIELDS setting controls which fields should be
    exposed to the client.
    """
    collection = result_dict.keys()[0].split('_')[0]
    results = {}
    for field in app.config['SEARCH_VIEW_FIELDS']:
        result_field = '%s_%s' % (collection, field)
        if result_field in result_dict:
            results.update({field: result_dict['%s_%s' % (collection, field)]})

    # We don't need to return datetimes or timestamp, so format time here
    if 'publication_date' in results and results['publication_date']:
        results['publication_date'] = results['publication_date']\
            .strftime('%b. %d, %Y')

    return results


def get_bookmarks(user_id):
    bookmarks = Bookmark.query\
                    .filter_by(user_id=user_id)\
                    .order_by(db.desc(Bookmark.timestamp))\
                    .all()
    results = []
    for bookmark in bookmarks:
        bookmark = bookmark.__dict__
        bookmark['timestamp'] = bookmark['timestamp'].strftime('%b. %d, %Y %H:%M')
        del(bookmark['_sa_instance_state'])
        results.append(bookmark)

    return results


def get_solr_interface(collection):
    """Return an interface to a given collection"""
    return sunburnt.SolrInterface('%s/%s' % (app.config['SOLR_URL'],\
        collection), schemadoc='%s/%s.xml' % (app.config['SCHEMA_DIR'],\
        collection))

app.wsgi_app = ProxyFix(app.wsgi_app)
