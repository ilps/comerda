# Copyright 2013 ILPS group UvA (Information and Language Processing
# Systems group University of Amsterdam) 
# 
# Licensed under the Apache License, Version 2.0 (the "License"); you
# may not use this file except in compliance with the License. You may
# obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied. See the License for the specific language governing
# permissions and limitations under the License.
class Config(object):
    DEBUG = True

    # The name of the application which will be displayed in the user interface
    APPLICATION_TITLE = 'CoMeRDa'

    # Flask's secrete key used to sign cookies
    SECRET_KEY = ''

    # The URL on which the user interface and assets are reachable
    SITE_URL = ''

    # The URL used by the client side to obtain data
    WEB_API_URL = SITE_URL

    # The URL of the Solr instance used for searching collections
    SOLR_URL = 'http://localhost:8999/solr'

    # The absolute path to the directory where solr schema's are stored on startup
    SCHEMA_DIR = '/absolute/path/to/schemas'

    # Description of the collections that are available through Solr,
    # for example:
    # COLLECTIONS = {
    #   'fotovlop': { # unique id of the collection
    #       'name': 'Sound and Vision Photo Collection', # human readable name
    #       'type': 'photo', # type of documents in collection ('photo' or 'text')
    #       'facets': ['fotovlop_publication_date', 'fotovlop_contributor', 'fotovlop_names'], # names of available facets
    #       'media_url': 'http://comerda.com/static/fotovlop'
    #   },
    # }
    COLLECTIONS = {}

    # Order in which collections should be presented in the UI, for
    # example:
    # COLLECTION_ORDER = ['newspapers', 'fotovlop']
    COLLECTION_ORDER = []

    # Description of available facets, for example:
    # FACETS = {
    #     'fotovlop_keywords': { # unique id for the facet with a collection
    #         'name': 'Keywords', # human readable name
    #         'description': '',
    #         'type': 'checkbox' # type of facet ('checkbox' or 'range')
    #     }
    # }
    FACETS = {}


    # Fields to expose to the client in basic search view, for example:
    # SEARCH_VIEW_FIELDS = ('docid', 'title', 'description')
    SEARCH_VIEW_FIELDS = ()

    # Fields to expose to the client in document view
    DOCUMENT_VIEW_FIELDS = SEARCH_VIEW_FIELDS

    # Number of hits to display on a single page
    PAGINATION_HITS_PER_PAGE = 10

    # Field(s) to use for highlighting; %s is the collection. 'text' should be
    # appended separately, for example:
    # HIGHLIGHT_FIELDS = ['%s_title', '%s_content']
    HIGHLIGHT_FIELDS = []

    # Enable user auth if there is an external system in place that
    # handles user authentication and redirection to the comerda interface
    ENABLE_USER_AUTH = True

    # The url of the page used for logging into the system
    REDIRECT_LOGIN_URL = ''

    # The url of the page to which the user should be redirected after
    # logging out of the system
    REDIRECT_LOGOUT_URL = ''

    # The url of an external javascript file that can be used to modify
    # bahaviour of the interface (optional).
    EXTERNAL_JS = ''

    # Optional URL of the about page to which a link is displayed within
    # the user interface
    ABOUT_URL = ''

    # MySQL db where in which bookmarks and users will be stored
    SQLALCHEMY_DATABASE_URI = 'mysql://username:password@hostname/database'

    # Event and error logging using Sentry
    ENABLE_SENTRY_LOGGING = False
    SENTRY_DSN = ''

    # ILPSLogging integration
    ENABLE_ILPSLOGGING = False
    ILPSLOGGING_API_URL = ''
    ILPSLOGGING_PROJECT_KEY = ''

    # Allow any settings to be defined in local_settings.py which should be
    # ignored in your version control system allowing for settings to be
    # defined per machine.
    try:
        from local_settings import *
    except ImportError:
        pass
