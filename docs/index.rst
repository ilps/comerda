.. Comerda API documentation master file, created by
   sphinx-quickstart on Fri Aug 14 10:50:57 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Comerda API's documentation
===========================

.. toctree::
   :maxdepth: 2

The comerda exploratory search engine has a typical front-end and back-end
setup. The front-end is the interface that user interacts with. The back-end
contains the search engine and the indexed collections and exposes a REST api.
This api is used by the front-end, but any program can use it. This document
describes the exposed endpoints and their usage.


Document
--------

This endpoint will provide a detailed view of a single document in a given
collection. If query terms are supplied this will also perform term highlighting
in the text of the document.

.. http:post:: /document

   Fetch a single document with query term highlighting.

   :query string docid: 
   :query string collection: 
   :query string queryterms: 


Search
------

Comerda provides three main modes of search: 1) plain single-collection search,
2) multi-collection comparative search and 3) similarity search.

Both the single- and multi-collection search in the interface use same endpoint:
`query/basic`. The similarity search is provided be the second endpoint
`query/similarity`.

.. http:post:: /query/basic

    The main search endpoint, this provides search, facet (filter) and
    pagination functionality.

    **Example request**

    .. sourcecode:: http

        curl -XPOST $API_URL/query/basic --data "collection=greylit&query=flint&page=0&filters=%7B%22greylit_contributor%22%3A%5B%22Everett%2C+L.%22%5D%2C%22greylit_names%22%3A%5B%22Suffolk+County+Council+Archaeological+Serice%22%5D%2C%22greylit_country%22%3A%5B%22ENGLAND%22%5D%2C%22greylit_publication_date%22%3A%5B%222010-08-09%22%2C%222011-01-19%22%5D%7D"

    **Example response**

    .. sourcecode:: http

        {
          "docs": [
            {
              "content": "ARCHAEOLOGICALARCHAEOLOGICAL...",
              "country": "ENGLAND",
              "county": "SUFFOLK",
              "district": "BABERGH",
              "docid": "7,851",
              "site": "HAD 085  and  HAD 089 Land between Lady Lane and Tower Mill Lane",
              "title": "Land between Lady Lane and Tower Mill, Lane, Hadleigh"
            },
            ...
          ],
          "facets": {
            "greylit_contributor": [
              [
                "Everett, L.",
                5
              ]
            ],
            ...
            "greylit_publication_date": [
              [
                "2010-10-15T00:00:00Z",
                5
              ],
              [
                "2010-10-15T00:00:00Z",
                5
              ]
            ],
            ...
          },
          "snippet": {
            "7,851": {
              "greylit_content": [
                " quantities499.CBM fabric & form4910. <em>Flint</em> catalogue5011. Animal bone by context53List of Plates1. Environmental Sequence 1362. Environmental Sequence 2373. View of the trench cut through the"
              ]
            },
            ...
          },
          "totalhits": 5
        }

    :query string query: query string entered by the user
    :query string collection: collection to search through
    :query object filters: JSON object specifying the filters for the facets
    :query int page: specify a result page other than the first one (pagination)
    :query int hits_per_page: number of results per page


.. http:post:: /query/similarity

    Perform a similarity search based on a specified document.

    Source and target collection are required in order to obtain the proper
    document, and query the appropriate collection from which we require similar
    documents.

    Hits per page allows us to specify how many documents we actually want to
    retrieve, and defaults to the amount specified in the settings.

    Page allows for pagination.

    Filters is for faceting.

    :query string docid:
    :query string source_collection:
    :query string target_collection:
    :query object filters: JSON object specifying the filters for the facets
    :query int page: specify a result page other than the first one (pagination)
    :query int hits_per_page: number of results per page


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

